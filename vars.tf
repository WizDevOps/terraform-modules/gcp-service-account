variable project_id {
  description = "GCP Project ID"
  type        = string
}

variable name {
  description = "Max 28 characters name for the service account. Make it meaningful!"
  type        = string
}

variable description {
  description = "An optional description for the service account"
  type        = string
  default     = ""
}

variable roles {
  description = "The name of the GCP IAM project roles to assign to the service account"
  type        = list(string)
}

variable gke_workload_identity_sa {
  description = "The name of the GKE Workload identity service accounts to allow to act as the service account"
  type        = list(string)
  default     = []
}

variable generate_key {
  type    = bool
  default = false
}
