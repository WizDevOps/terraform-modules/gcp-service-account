# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2020-01-12
### Added
- support for Workload Identity
- Google Service Account (GSA) Private Key generation

### Fixed
- broken links in CHANGELOG

## [0.0.1] - 2019-09-07
### Added
- CHANGELOG + LICENCE + README

[Unreleased]: https://gitlab.com/WizDevOps/terraform-modules/gcp-service-account/compare/v0.1.0...master
[0.1.0]: https://gitlab.com/WizDevOps/terraform-modules/gcp-service-account/compare/v0.0.1...v0.1.0
[0.0.1]: https://gitlab.com/WizDevOps/terraform-modules/gcp-service-account/tree/v0.0.1