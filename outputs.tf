output "email" {
  value = google_service_account.default.email
}

output "key" {
  value = data.template_file.key.rendered
}

data "template_file" "key" {
  template = "$${key}"
  vars = {
    key = var.generate_key ? google_service_account_key.default.0.private_key : ""
  }
}